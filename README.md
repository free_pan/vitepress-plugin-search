# @pzy915/vitepress-plugin-search

`vitepress-plugin-search`是一个实现vitepress文章离线搜索的插件. 此项目基于 [vitepress-plugin-search: 1.0.4-alpha.19](https://github.com/emersonbottero/vitepress-plugin-search.git) 修改而来. 


解决的问题/增加的特性:

1. 解决当 vitepress(版本:1.0.0-alpha.35) 存在 `base` 设置时, 搜索结果的跳转连接包含两个`base`值问题
2. 让索引文档的文档内容包含md文件名(不含`.md后缀`)(`index.md这种文件名除外`)。因为本人文章的h1使用的是vitepress变量`{{$frontmatter.title}}`实际值就是文件名, 插件默认的索引内容并未存储文件名，导致输入文件名进行搜索，无法搜索到内容

## 解决中文搜索问题

### 方式一: tokenize 设置为full

```shell
pnpm i @pzy915/vitepress-plugin-search -D
```

```ts
import { ConfigEnv, defineConfig } from 'vite'
import vueJsx from '@vitejs/plugin-vue-jsx'
import { SearchPlugin } from '@pzy915/vitepress-plugin-search'

export default defineConfig((env: ConfigEnv) => {
  return {
    plugins: [
      vueJsx(),
      SearchPlugin({
        previewLength: 20,
        buttonLabel: '搜索',
        placeholder: '文章搜索',
        tokenize: 'full',
      }),
    ],
    server: {
      fs: {
        // Allow serving files from one level up to the project root
        allow: ['../..'],
      },
      host: '0.0.0.0',
      // open: true,
    },
  }
})
```

### 方式二: 使用结巴分词器(好像没起作用)

```shell
pnpm i @node-rs/jieba @pzy915/vitepress-plugin-search -D
```

`vite.config.ts`

```ts
import { ConfigEnv, defineConfig } from 'vite'
import vueJsx from '@vitejs/plugin-vue-jsx'
import { SearchPlugin } from '@pzy915/vitepress-plugin-search'
import { load, cut } from '@node-rs/jieba'

export default defineConfig((env: ConfigEnv) => {
  return {
    plugins: [
      vueJsx(),
      /**
       * flexsearch 配置: https://github.com/nextapps-de/flexsearch#options
       *
       * vitepress-plugin-simple-search 配置: https://github.com/stuyk/vitepress-plugin-simple-search
       *
       * @return  {[type]}  [return description]
       */
      SearchPlugin({
        previewLength: 20,
        buttonLabel: '搜索',
        placeholder: '文章搜索',
        /**
         * 采用分词器优化.
         *
         * 中文分词器：https://www.npmjs.com/package/@node-rs/jieba
         *
         * 字典配置：https://www.npmjs.com/package/nodejieba
         *
         * 相关文章: https://zhuanlan.zhihu.com/p/453803476
         */
        tokenize: function (str) {
          return cut(str, false)
        },
      }),
    ],
    server: {
      fs: {
        // Allow serving files from one level up to the project root
        allow: ['../..'],
      },
      host: '0.0.0.0',
      // open: true,
    },
  }
})
```

## Provides local search to your documentation site

Uses [flexsearch](https://github.com/nextapps-de/flexsearch).

## Installing

```js
npm i vitepress-plugin-search markdown-it flexsearch -D
```

## Add the plugin

```js
//vite.config.ts
import { SearchPlugin } from "vitepress-plugin-search";
import { defineConfig } from "vite";

//default options
var options = {
  ...flexSearchIndexOptions,
  previewLength: 62,
  buttonLabel: "Search",
  placeholder: "Search docs",
};

export default defineConfig({
  plugins: [SearchPlugin(options)],
});
```

## Options

Accept [FlexSearch Index Options](https://github.com/nextapps-de/flexsearch#options)

## Multi language support

Provided by flexsearch  
  
See [chinese settings for example](https://github.com/emersonbottero/vitepress-plugin-search/issues/11)
